<?php

namespace Lyrikz\BugTrackerBundle\Controller;

use Lyrikz\BugTrackerBundle\Form\BugTrackerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * BugTracker controller.
 *
 * @package Lyrikz\BugTrackerBundle\Controller
 * @Route("/bug")
 */
class BugTrackerController extends Controller
{
    /**
     * @Route("/submit", name="bug_submit")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function submitAction(Request $request)
    {
        $form = $this->createForm(BugTrackerType::class);
        $formView = $form->createView();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid() && $form->isSubmitted()) {
                try {
                    $clientIp = $request->getClientIp();
                    $data = $form->getData();
                    $bug_manager = $this->get('lyrikz.bug_tracker.manager');
                    $bug_manager->submit($data, $clientIp);
                    $this->addFlash('success', 'bug.flash.success');
                } catch (\Exception $e) {
                    $this->addFlash('warning', $e->getMessage());
                }
            }
        }

        return $this->render(
            'LyrikzBugTrackerBundle:Bug:submit.html.twig',
            [
                'form' => $formView,
            ]
        );
    }

    /**
     * @Route("/projects", name="lyrikz_bug_list")
     */
    public function listProjectsAction()
    {
        $projects = $this->get('lyrikz.bug_tracker.manager')->getProjects();
        return new Response(dump($projects));
    }
}
