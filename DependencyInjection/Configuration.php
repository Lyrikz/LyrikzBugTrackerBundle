<?php

namespace Lyrikz\BugTrackerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package Lyrikz\BugTrackerBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('lyrikz_bug_tracker');

        $rootNode
            ->children()
                ->arrayNode('api')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->children()
                        ->integerNode('project_id')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        ->integerNode('assignee_id')
                        ->end()
                        ->integerNode('milestone_id')
                        ->end()
                        ->scalarNode('token')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}