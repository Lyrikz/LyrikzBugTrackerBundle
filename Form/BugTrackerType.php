<?php

namespace Lyrikz\BugTrackerBundle\Form;

use Lyrikz\BugTrackerBundle\Manager\BugTrackerManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class BugTrackerType
 * @package Lyrikz\BugTrackerBundle\Form
 */
class BugTrackerType extends AbstractType
{
    /**
     * @var array
     */
    private $labels;

    /**
     * BugTrackerType constructor.
     * @param BugTrackerManager $bugTrackerManager
     */
    public function __construct(BugTrackerManager $bugTrackerManager)
    {
        $this->labels = $bugTrackerManager->getLabels();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title')
            ->add('description', TextareaType::class)
            ->add(
                'labels',
                ChoiceType::class,
                [
                    'choices' => $this->labels,
                    'multiple' => true,
                    'choices_as_values' => true,
                ]
            );
    }
}
