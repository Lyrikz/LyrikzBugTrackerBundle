<?php

namespace Lyrikz\BugTrackerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class LyrikzBugTrackerBundle
 * @package Lyrikz\BugTrackerBundle
 */
class LyrikzBugTrackerBundle extends Bundle
{
}
