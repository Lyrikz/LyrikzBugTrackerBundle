<?php

namespace Lyrikz\BugTrackerBundle\Manager;

use Gitlab\Client;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class BugTrackerManager
 * @package Lyrikz\BugTrackerBundle\Manager
 */
class BugTrackerManager
{
    /**
     * @var
     */
    private $config;

    /**
     * @var integer
     */
    private $project_id;

    /**
     * @var integer
     */
    private $assignee_id;

    /**
     * @var integer
     */
    private $milestone_id;

    /**
     * @var string
     */
    private $token;
    /**
     * @var TokenStorageInterface
     */
    private $user;

    /**
     * BugManager constructor.
     * @param $config
     * @param TokenStorageInterface $tokenStorageInterface
     */
    public function __construct($config, TokenStorageInterface $tokenStorageInterface)
    {
        $this->config = $config;
        $this->user = $tokenStorageInterface;
        $this->getConfig();
        $this->getUser();
    }

    /**
     * @param array $data
     * @param $clientIp
     */
    public function submit(array $data, $clientIp)
    {
        $params = $this->handleData($data, $clientIp);
        $client = $this->getClient();
        $client->api('issues')->create($this->project_id, $params);
    }

    /**
     * @return array
     */
    public function getLabels()
    {
        $client = $this->getClient();
        $labels_api = $client->api('projects')->labels($this->project_id);

        $labels = [];
        foreach ($labels_api as $label) {
            $label_key = strtolower($label['name']);
            $label_key = str_replace(' ', '', $label_key);
            $label_key = 'bug.'.$label_key;
            $labels[$label_key] = $label['name'];
        }

        return $labels;
    }

    /**
     * @param array $data
     * @param $clientIp
     * @return array
     */
    private function handleData(array $data, $clientIp)
    {
        $labels_array = $data['labels'];
        $labels = '';
        for ($i = 0; $i < count($labels_array); $i++) {
            if ($i == count($labels_array) - 1) {
                $labels .= $labels_array[$i];
            } else {
                $labels .= $labels_array[$i].',';
            }
        }
        $description = $data['description'];
        $description .=
' // Username : Lyrikz // Client IP : '.$clientIp;

        return $params = [
            'title' => $data['title'],
            'description' => $description,
            'assignee_id' => $this->assignee_id,
            'milestone_id' => $this->milestone_id,
            'labels' => $labels,
        ];
    }

    /**
     * @return mixed
     */
    public function getProjects()
    {
        $client = $this->getClient();

        return $client->api('projects')->accessible();
    }

    /**
     * @return $this
     */
    private function getClient()
    {
        $client = new Client('https://gitlab.com/api/v3/');

        return $client->authenticate($this->token, Client::AUTH_URL_TOKEN);
    }

    /**
     *
     */
    private function getConfig()
    {
        $this->token = $this->config['api']['token'];
        $this->project_id = $this->config['api']['project_id'];

        if (isset($this->config['api']['assignee_id'])) {
            $this->assignee_id = $this->config['api']['assignee_id'];
        }
        if (isset($this->config['api']['milestone_id'])) {
            $this->milestone_id = $this->config['api']['milestone_id'];
        }
    }

    /**
     *
     */
    private function getUser()
    {
        if ($this->user->getToken()) {
            $this->user = $this->user->getToken()->getUser();
        }
    }
}
